# JavaScript Docker React
A React app that runs in a Docker container using Nginx.  Uses multiple environment files and deployment scripts.

[Environment Variables](https://create-react-app.dev/docs/adding-custom-environment-variables)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Create environment files (below) from this [template](.env.template).
- Start the app: `npm start`

### Environment Files
|File                   |Description               |
|-----------------------|--------------------------|
|.env.development.local |Used for local development|
|.env.development       |AWS DEV Environment       |
|.env.production        |AWS PROD Environment      |

# Deployment
Ensure Docker Desktop or daemon is running.
- DEV: [devops-dev-deploy.bat](./devops-dev-deploy.bat)
- PROD: [devops-prod-deploy.bat](./devops-prod-deploy.bat)

# Notes
- `npm run build` creates an optimized version of the app that can be run in DEV, QA, UAT, or PROD.
- `npm start` uses `.env.development` and sets `NODE_ENV` to `development`.
  - `.env.development.local` overrides variables from `.env.development`
  - `.env.production.local` overrides variables from `.env.production`
- `npm run build` calls `react-scripts build` and uses `.env.production` and sets `NODE_ENV` to `production` 
- `npm test` sets `NODE_ENV` to `test`
- Envionmental variables in a React App [must star](https://create-react-app.dev/docs/adding-custom-environment-variables) with `REACT_APP_`
