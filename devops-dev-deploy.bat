copy .env.production .env.production.backup
copy .env.development .env.production

docker build --no-cache -t react-app:latest .

aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 894608134501.dkr.ecr.us-east-2.amazonaws.com
docker tag react-app:latest 894608134501.dkr.ecr.us-east-2.amazonaws.com/test-api:latest
docker push 894608134501.dkr.ecr.us-east-2.amazonaws.com/test-api:latest
aws ecs update-service --desired-count 1 --cluster vc-dev-ecs-cluster --service test-api-service --force-new-deployment --task-definition arn:aws:ecs:us-east-2:894608134501:task-definition/test-api:12

copy .env.production.backup .env.production
del .env.production.backup
