docker build --no-cache -t react-app:latest .

aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 894608134501.dkr.ecr.us-east-2.amazonaws.com
docker tag react-app:latest 894608134501.dkr.ecr.us-east-2.amazonaws.com/vc-prod-test-api:latest
docker push 894608134501.dkr.ecr.us-east-2.amazonaws.com/vc-prod-test-api:latest
aws ecs update-service --desired-count 1 --cluster vc-prod-ecs-cluster --service vc-prod-test-api --force-new-deployment --task-definition arn:aws:ecs:us-east-2:894608134501:task-definition/vc-prod-test-api:8
