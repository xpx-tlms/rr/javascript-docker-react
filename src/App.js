import './App.css'

function App() {
  return (
    <div className="App">
        <p> NODE_ENV: { process.env.NODE_ENV } </p>
        <p> Environment Name: { process.env.REACT_APP_ENV_NAME } </p>
    </div>
  )
}

export default App;
